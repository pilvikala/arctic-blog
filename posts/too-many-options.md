---
title: Too many options
description: The UX of choice
date: 2017-05-01
tags:
  - user experience
  - software development
layout: layouts/post.njk
---

My recent visit to Subway reminded me again why I prefer to avoid this fast food restaurant. It is not about their subs, or what they put in them. I actually like the combination of tastes, but I hate the process I have to go through to get the actual sub.

At first, it looks so simple. They have a board with pictures of 16 different types of subs, and I can see the price I am going to pay. The lady asks me what I want, and I tell her I want the Italian BMT. Clear and simple. But then…

… I have to choose which type of bread I want. I did not expect it, but it’s actually not difficult to choose the type of bread, is it?

And then she asks me what I want to put in it.

Well, I want THE Italian BMT. I want to replicate the image in the picture. I don’t want to guess what’s in it! So, I calmly explain I want the Italian BMT. She nods, puts some ingredients on the bread, and puts the bread in a grill.

21 seconds later, another lady comes and asks me what I want in my sub. Come on! I want the Italian BMT, and I’m confused because I am not sure what’s not clear on wanting the exact sub that is displayed in the picture above her head. She explains that I can pick anything I want, but I am not really in a mood to pick my ingredients in order to experience the true Italian taste, and there’s a long queue of other people behind me, and they all suddenly look nervous because they have to wait for me to decide what I actually want. Because of this, they will be late for work and the GDP will go down by 20% because I am so slow and cannot guess what’s inside the proper Italian BMT Sub!

I resign, tell her I want tomatoes, leaf salad, and jalapeños, a combination that is NOT Italian at all but I don’t care at this point, I just want to go home, get in bed, and cry and pity myself for bringing down the already-on-its-knees Finnish economy.

She asks me what sauce I want in my sub, but notices the pale color of my face and immediately recommends one to go with it so that I won’t really have to choose here, and my life is getting much better again.

Then I finally pay, sit down in a dark corner, and eat my sub quietly.

…

I am sharing this story because I can see there is a parallel with software. It’s the dark place where we, the people developing software, offload all of our decision making. Instead of deciding a problem for a user, we let the user decide what they want. Not sure how this small thing should behave? Having too many different opinions and each one seems valid? Let’s put it to Application Settings so the users can decide on their own!

Next time, when adding a new user option, remember the Subway story, or visit Subway and experience it on your own, and then get back to review your original user story and its goals to decide what the right option for your customer is to save them the trouble and offer them the right sauce.


PS: Do you know Subway has a help page on how to order a sub? I don’t feel like I should need to read a manual before ordering a sandwich: http://www.subway.fi/en/products/how-to-order-your-sub
