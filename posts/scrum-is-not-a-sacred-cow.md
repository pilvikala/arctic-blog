---
title: Scrum is not a sacred cow
description: This is a post on My Blog about agile frameworks.
date: 2016-05-01
tags:
  - scrum
  - agile
  - software development
layout: layouts/post.njk
---
What benefits do you see in having sprints?

This was the question I asked one of my teams couple weeks ago. A half an hour long discussion rendered just one answer: “None”. And so we decided to try working on our project without the deadlines set by the timebox of the sprint.

When I told others about this, I was told the idea was stupid. People need deadlines, I heard. Without the deadlines, they would just hang around and did nothing and the overall morale of the team would collapse.

We tried it anyway - we cancelled the sprints while keeping the other artifacts of scrum like daily standups, bi-weekly retrospectives, or bi-weekly demos for stakeholders. After a while, I talked with the team to gather feedback on this change. Here it is:

Pros:
1) The obvious one is sprint planning - or the lack of it. This saves some amount of time. The standard backlog refinement meetings (backlog grooming) are still happening and we talk about the details of the stories there. After the refinement, they are usually ready to be taken by anyone in the team and implemented (according to priorities).

2) Before, we often had discussions about accepting stories similar to this one:
Me: “I like it, but there’s one little refinement I would like you to do before accepting this story”.
Team member: “It’s not in the acceptance criteria and we can’t make it in this sprint. Can you open a new story and put it in the next sprint?”
Now we continue to work on the story until both I and the team are satisfied. We do small tweaks, sometimes even vet the UI with the designers, and nobody is complaining about something missing in the acceptance criteria.

3) The above actually leads to a better quality. There is no pressure at the end of the sprint to finish all the commitment that was previously based on estimates. The work is done when it is done. This also removes all the long conversation about what would be the way to finish something by the deadline.

4) The above also killed the dead time just before the end of the sprint. Parkinson’s law has no effect here to fill in the remaining time with more work - we just continue working on what is in our backlog.

5) Team uses common sense more - I am not sure what the actual cause is here. But it appears that the removal of the pressure on the deadline removed the need to meet just the specific acceptance criteria. People think more about the actual purpose of the story and even if I omit something, they come back and tell me what I wrote does not make sense.

6) The team feels more relaxed (but the average velocity did not drop down).

Cons:
1) I have to pay more attention to the size of the stories. They need to be small enough to fit into a week so I can keep tracking progress, and I actually need couple stories to get implemented during the week. This means a bit more work and a bit more thinking for me.


2) Some people feel that the missing deadlines make them less productive. However, there is currently no evidence for this, it is just a feeling.


3) Some people feel we have been just lucky that it has worked.


4) There is a concern that in a long term, this may feel like a marathon with no specific milestone. We are trying to mitigate this by having regular demos to stakeholders, betas, and we will have to release a product eventually.


To me, being agile is about the ability to improve on what you are doing and adapt your process so it makes you more productive. Using scrum retrospectives to find out sprints are causing friction and therefore they need to be cancelled is perfectly legal - we removed the major obstacle to our productivity.

The point of this post is not a suggestion you should cancel sprints. It may not even work for you. As the title says, scrum is not a sacred cow. Stop following it blindly, question it, and find and remove the part that is causing the biggest friction, even if it undermines the whole process.
