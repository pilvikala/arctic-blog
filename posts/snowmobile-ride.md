---
title: Snowmobile ride
date: 2023-04-29
tags:
  - life in the north
  - nature
  - snow
layout: layouts/post.njk
---

Using a snowmobile, you can get to places that are otherwise impossible to visit.

{{ "https://www.youtube.com/embed/0IX6TTNirHs" | youtube | safe }}
