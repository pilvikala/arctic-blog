---
title: Language specifics
description: About cultural differences
date: 2017-03-03
tags:
  - user experience
  - ux
layout: layouts/post.njk
---
You may decide that the only language your software will ever support will be English but that will not save you from the traps of differences between UK and US English. Here’s what I found today when describing an existing UI behavior:

“For example, searching for "yellow” will show projects with traffic light set to amber.“

Now what?
