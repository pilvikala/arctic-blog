---
title: New project - Recipe Book
date: 2023-05-17
tags:
  - product management
  - software
  - recipe book
  - life in the north
layout: layouts/post.njk
---

I have a new side project: [A recipe book](https://www.my-recipe-book.app).

Since the pandemic started, we kind of changed our habits of going to a store every week for a load of food for the upcoming week. We thought we could reduce the time spent among other (infectious) people by shopping only once a month. Sure, you still need some fresh produce, such as vegetables and fruits, every week, but that limits the store visit to ten minutes.

Since we moved to the north, the need to do a one monthly shopping trip has increased even more. The store with good prices is around 100 km away and it takes an hour to get there (if the weather permits). So every month, I take my small Ford Fiesta, drive to the store, load the car with food and drive back home. It takes half a day but then I have the other weekends free.

This approach to shopping required us to change the way we plan meals. We actually need to sit down and plan what we're going to cook each weekend until the end of the month and make sure we have all the supplies. We were looking for a tool that could store our favorite recipes, include ingredients, and plan meals in a calendar. It should also be free and available on all platforms.

We found one that wasn't free and we almost started using it but then we learned it requires a payment on each platform (PC and Android separately). So as product managers do, I said I could code something like this over a weekend and picked up this new hobby.

This happened couple months ago (ha ha) but now I'm ready to share it with the world. It's ugly, but it works. Also, it's free (for now). And I learned a lot new stuff about React, NextJS, Google authentication, and that I also don't really want to develop mobile apps.
