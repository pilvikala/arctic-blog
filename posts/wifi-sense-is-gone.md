---
title: Wi-Fi Sense is gone
description:
date: 2017-08-24
tags:
  - product management
  - asking questions
  - why
  - software development
layout: layouts/post.njk
---
Finally! I wonder what made product managers in Microsoft to think this would be a good idea.

For those who do not know, Wi-Fi Sense is (was) a new feature in Windows 10 that allowed you share your Wi-Fi credentials with your Facebook, Skype, and Outlook contacts.

Now Microsoft said people had not used it so the feature was removed from Windows in the latest Anniversary Update.

I just cannot understand how this idea was not killed shortly after its inception. I mean, there are multiple ways you can validate your ideas - asking your users, potential customers, comparing the feature with the goals you are trying to achieve, and so on. Who were the people who said “yeah, I would LOVE to share my home Wi-Fi password with all of my 155* Facebook friends!”. What could have been the goals Microsoft had this feature would help to reach them? Or was this a result of the lean software development? Did they want to make this bigger so they made a small step to try whether the feature succeeds so they could develop it further?

Can you imagine what the “further” could be? Sounds quite scary to me…


* average number of facebook friends as of Jan 2016 according to this page: http://www.telegraph.co.uk/news/science/science-news/12108412/Facebook-users-have-155-friends-but-would-trust-just-four-in-a-crisis.html
