---
title: Estimates
description:
date: 2017-09-01
tags:
  - product management
  - "#noestimates"
  - software development
layout: layouts/post.njk
---

![Chart]({{'/img/post-images/estimates.png' | url }})

I collected statistics on “how good my team is in estimating”. The result: not good at all.

At the moment, the only estimate we make is whether a story is big and needs splitting. I measure throughput in stories per week.
