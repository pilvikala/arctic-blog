---
title: Change the world or do nothing
description: Always ask why
date: 2016-08-24
tags:
  - product management
  - asking questions
  - why
  - software development
layout: layouts/post.njk
---
I have just read an article about Washington, DC’s useless streetcar service and there was one very interesting part that caught my eye:

>*…Washington’s planners seem to have been so fixated on the low price tag that they have not asked themselves the central question that should guide any transit plan: will it actually be useful in moving people?*

(source: http://www.economist.com/blogs/gulliver/2016/02/streetcar-mire)

You could say this is a typical government project. But isn’t this something that is happening in your company too? How often do you ask yourself how this user story is going to be useful to the users when it is implemented?

When implementing a new feature, there is usually a user in the beginning that says what she needs in your software (let’s call her Susan).  Susan usually says things like “I need this apple to be blue”. Right at this point, you have two options:

a) Go and tell the team to paint the apple with a blue color.
b) Ask why.

I know many people in many companies that paint apples blue. The streetcar service in Washington DC is an apple painted blue. I think this is happening because people tend to not oppose to someone who is offering to pay money for something to be done. It might be because asking “why” may (and often does) lead to “I don’t know, you’re right this is stupid, let’s cancel this project” type of answer effectively meaning no money for the company that asked. And also, the “why” may seem as direct “no” which makes people angry (your boss being one of them).

But if you do not ask why you will never find out what Susan actually needs. When you give her the blue apple in the end, she will find out it is not what she had needed and while she still may pay you for the project, she will not ask you to run another one for her.

For option B, you can start with the simple “How is this going to change what you are doing now?” The change is important. Most of the time, Susan is just trying to make her own life easier by introducing a change in it. A change, that will save her time, money, or a change that will give her a special benefit. You know, Susan might actually be the Evil Queen and what she really needs is this:

![Apple]({{'/img/post-images/apple.jpg' | url}})
*(source: http://disney.wikia.com/wiki/File:Snow-white-disneyscreencaps.com-7307.jpg)*

Or maybe she just needs to move Snow White out of the country!

Asking the right questions will help you finding out what Susan’s actual goals are and only after that you can help her achieving them. Jeff Patton, the author of the User Story Mapping book calls it “changing the world for the user”:

“Every great idea you turn into a product solution changes the world in some small, or not-so-small, way for the people who use it. In fact, if it doesn’t, you’ve failed.
(Jeff Patton, User Story Mapping, loc. 383).

I say, change the world, or do nothing. There’s no other option.
