---
title: Google Calendar Event Colorizer
description: A simple script that colorizes events in your Google Calendar
date: 2021-10-12
tags:
  - code
  - tools
  - open source
layout: layouts/post.njk
---

I prefer to see important events (i.e. events with customers) in my calendar with a different color so I notice them in time and get prepared. I used to edit the invitations manually to set the color but I often realized I forgot to do that until it was too late.

Here’s a little script that helps me colorize the events automatically. It sets the predefined color on every calendar event with guests from outside of my company: https://gitlab.com/pilvikala/gcal-event-colorizer/-/blob/main/index.js

How to use it:

  1. Go to https://script.google.com/home
  1. Click ‘New script’
  1. Paste the script from index.js
  1. Read the comments in the top of the index.js file and update the variables to suit your needs.
  1. Save the project and give it a name
  1. Run the project. It will ask you to give the script permissions to access your calendar.
  1. In the triggers tab, add a trigger to run this periodically
