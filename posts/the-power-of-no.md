---
title: The power of no
description: How to say no without saying no
date: 2019-09-21
tags:
  - product management
  - software development
layout: layouts/post.njk
---

I am very bad at saying No. It is an issue if you consider the fact I am a product owner, but I really have a hard time saying No to people who want new features or who desperately need some bugs to be fixed. I am a product owner that wants everyone to be happy.

At the company I worked with in the past few months, I was basically the one who said what features we would implement and what bugs we would fix next. We had a long-term strategy I had to comply with, but those tiny details affecting hundreds of users were for me to decide on.

I had requests for changes, new features, and bug fixes coming in all the time. Most of them were from our customer success representatives - the people who were providing support to our end users. They knew what the long-term strategy was and they knew what we were planning to work on in the near future, but they always tried to push “just one more thing” in.

Could I say no? Of course I could, and I actually tried that multiple times, but I was often overruled and my short-term plans became mid-term plans and the long-term strategy became only a vision.

One day when another urgent request for a bigger feature came from one of my favorite customer success managers, I tried a new approach. I laid my cards on the table and told him that these were all the things we were planning in the upcoming months, and most of them were requests coming from him and he had said they had been urgent. I asked him to help me to figure out where his new request should be squeezed in and what were the bug fixes we would postpone due to this new one. The result was amazing: he said his new urgent request could actually wait, the things we had been planning to fix were actually much more important.

That day I learned that the major stakeholders in the company suffered from poor memory. They always focused on what was most important for them at the moment but never looked at what was promised and planned before. Since that day, whenever someone has come to me with a request that seemed to be imperative to get done “right now” I asked them whether it was more important than what we were doing at that moment or what we had planned for the next few weeks. This eventually led to their satisfaction because they could have provided their input on what needed to be changed and they were able to influence how important the change was. It also reminded them what we were planning to do so, and thereby reassured them that we were (still) going the right direction.

And eventually, I was happy too because I no longer needed to say No.
