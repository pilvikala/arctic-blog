---
title: Fifty quick ideas...
description:
date: 2016-07-04
tags:
  - gojko
  - adzic
  - agile
  - user stories
  - software development
layout: layouts/post.njk
---

![Chart]({{'/img/post-images/fifty-ideas.jpg' | url }})

Fifty Quick Ideas To Improve Your User Stories is a really great book. What makes it even better is the set of hint cars (bought separately) you can carry with you to your grooming meetings.
